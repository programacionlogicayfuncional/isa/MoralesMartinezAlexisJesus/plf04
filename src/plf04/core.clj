(ns plf04.core)


(defn string-E-1
  [s]
  (letfn [(f [s s1]
            (if (empty? s)
              (if (and (>= s1 1) (<= s1 3))
                true 
                false)
              (if (= \e (first s))
                (f (rest s) (inc s1))
                (f (rest s) s1))))]
    (f s 0)))

(string-E-1 "Heelleeo")
(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hll")
(string-E-1 "e")
(string-E-1 "")

(defn string-E-2
  [s acc]
  (letfn [(f [s acc]
            (if (empty? s)
              (if (and (>= acc 1) (<= acc 3))
                true
                false)
              (if (and (= \e (first s)))
                (f (rest s) (+ acc 1))
                (f (rest s) acc))))]
    (f s acc)))

(string-E-2 "Heelleeo" 0)
(string-E-2 "Hello" 0)
(string-E-2 "Heelle" 0)
(string-E-2 "Heelele" 0)
(string-E-2 "Hll" 0)
(string-E-2 "e" 0)
(string-E-2 "" 0)

(defn string-times-1
  [s i]
  (letfn [(f [s i]
            (if (zero? i)
              ""
              (str s (string-times-1 s (dec i)))))]
    (f s i)))

(string-times-1 "plf04" 4)
(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [s i]
  (letfn [(f [s i acc]
            (if (zero? i)
              acc
              (str s (f s (dec i) (str acc)))))]
    (f s i "")))

(string-times-2 "plf04" 4)
(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

(defn front-Times-1
  [s i]
  (letfn [(f [s i]
            (if (< (count s) 3)
              (if (zero? i)
                ""
                (str s (f s (dec i))))
              (if (zero? i)
                ""
                (str (subs s 0 3) (f s (dec i))))))]
    (f s i)))

(front-Times-1 "plf04" 3)
(front-Times-1 "Chocolate" 2)
(front-Times-1 "Chocolate" 3)
(front-Times-1 "Abc" 3)
(front-Times-1 "Ab" 4)
(front-Times-1 "A" 4)
(front-Times-1 "" 4)
(front-Times-1 "Abc" 0)

(defn front-Times-2
  [s i acc]
  (letfn [(f [s i acc]
            (if (< (count s) 3)
              (if (zero? i)
                acc
                (f s (dec i)
                   (str s acc)))
              (if (zero? i)
                acc
                (f s (dec i)
                   (str (subs s 0 3) acc)))))]
    (f s i acc)))

(front-Times-2 "plf04" 3 "")
(front-Times-2 "Chocolate" 2 "")
(front-Times-2 "Chocolate" 3 "")
(front-Times-2 "Abc" 3 "")
(front-Times-2 "Ab" 4 "")
(front-Times-2 "A" 4 "")
(front-Times-2 "" 4 "")
(front-Times-2 "Abc" 0 "")

;opcion que se me ocurrio sin leftn
;(defn countXX-1
;  [s]
;  (if (= "" s)
;    s
;    (let [countXX-1 (fn [p] (= p [\x \x]))]
;      (count (filter countXX-1 (map vector s (rest s)))))))

;(countXX-1 "xxxx")
;(countXX-1 "abcxx")

;(defn countXX-2
;  [s acc]
;  (if (= "" s)
;    acc
;    (+ acc (let [countXX-1 (fn [p] (= p [\x \x]))]
;             (count (filter countXX-1 (map vector s (rest s))))))))
;(countXX-2 "xxxx" 0)

(defn count-XX-1
  [s]
  (letfn [(f [s s1]
            (if
             (empty? s)
              s1
              (if (and (= \x (first s))
                       (= \x (first (rest s))))
                (f (rest s) (inc s1))
                (f (rest s) s1))))]
    (f s 0)))

(count-XX-1 "plfxxxxx")
(count-XX-1 "abcxx")
(count-XX-1 "xxx")
(count-XX-1 "xxxx")
(count-XX-1 "abc")
(count-XX-1 "Hello there")
(count-XX-1 "Hexxo thxxe")
(count-XX-1 "")
(count-XX-1 "Kittens")
(count-XX-1 "Kittensxxx")

(defn count-XX-2
  [s]
  (letfn [(f [s s1 acc]
            (if (empty? s)
              acc
              (if (and (= s1 (first s))
                       (= s1 (first (rest s))))
                (f (rest s) s1
                   (inc acc))
                (f (rest s) s1 acc))))]
    (f s \x 0)))

(count-XX-2 "plfxxxxx")
(count-XX-2 "abcxx")
(count-XX-2 "xxx")
(count-XX-2 "xxxx")
(count-XX-2 "abc")
(count-XX-2 "Hello there")
(count-XX-2 "Hexxo thxxe")
(count-XX-2 "")
(count-XX-2 "Kittens")
(count-XX-2 "Kittensxxx")

(defn string-Splosion-1
  [s]
  (letfn [(f [x]
            (if (empty? x)
              x
              (apply str (f (subs x 0 (- (count x) 1))) x)))]
    (f s)))

(string-Splosion-1 "")
(string-Splosion-1 "Code")
(string-Splosion-1 "abc")
(string-Splosion-1 "ab")
(string-Splosion-1 "x")
(string-Splosion-1 "fade")
(string-Splosion-1 "There")
(string-Splosion-1 "Kitten")
(string-Splosion-1 "Bye")
(string-Splosion-1 "Good")
(string-Splosion-1 "Bad")

(defn string-Splosion-2
  [s acc]
  (letfn [(f [s acc]

            (if (empty? s)
              acc
              (f (subs s 0 (- (count s) 1)) (str acc s))))]
    (f s acc)))

(string-Splosion-2 "" "")
(string-Splosion-2 "Code" "")
(string-Splosion-2 "abc" "")
(string-Splosion-2 "ab" "")
(string-Splosion-2 "x" "")
(string-Splosion-2 "fade" "")
(string-Splosion-2 "There" "")
(string-Splosion-2 "Kitten" "")
(string-Splosion-2 "Bye" "")
(string-Splosion-2 "Good" "")
(string-Splosion-2 "Bad" "")

(defn array-123-1
  [xs]
  (letfn [(f [xs]
            (if (and
                 (= 1 (first xs))
                 (= 2 (first (rest xs)))
                 (= 3 (first (rest (rest xs)))))
              true (if (empty? xs)
                     false (f (rest xs)))))]
    (f xs)))

(array-123-1 [1 3 3 2 1 2 3])
(array-123-1 [1 1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 1 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])


(defn array-123-2
  [xs acc]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (str  false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1)
                         (== (first (rest xs)) 2)
                         (== (first (rest (rest xs))) 3))
                  (str true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f xs acc)))

(array-123-2 [1 2 3] 0)
(array-123-2 [1 3 3 2 1 2 3] 0)
(array-123-2 [1 1 2 3 1] 0)
(array-123-2 [1 1 2 4 1] 0)
(array-123-2 [1 1 2 1 2 3] 0)
(array-123-2 [1 1 2 1 2 1] 0)
(array-123-2 [1 1 3 1 2 3] 0)
(array-123-2 [1 2 3] 0)
(array-123-2 [1 1 1] 0)
(array-123-2 [1 2] 0)
(array-123-2 [1] 0)
(array-123-2 [] 0)

(defn string-X-1
  [s]
  (letfn [(f [s s1]
            (if (empty? s)
              ""
              (if (== 0 s1)
                (str (first s) (f (rest s) (inc s1)))
                (if (and (= \x (first s))
                         (> (count s) 1))
                  (f (rest s)
                     (inc s1))
                  (str (first s)
                       (f (rest s)
                          (inc s1)))))))]
    (f s 0)))

(string-X-1 "xplfxxx04xx")
(string-X-1 "xxHxix")
(string-X-1 "abxxxcd")
(string-X-1 "xabxxxcdx")
(string-X-1 "xKittenx")
(string-X-1 "Hello")
(string-X-1 "xx")
(string-X-1 "x")
(string-X-1 "")

(defn string-X-2
  [s]
  (letfn [(f [s x acc]
            (if (empty? s)
              acc
              (if (== 0 x)
                (str (first s) (f (rest s) (inc x) (str acc)))
                (if (and (= \x (first s))
                         (> (count s) 1))
                  (f (rest s) (inc x)
                     (str acc))
                  (str (first s)
                       (f (rest s)
                          (inc x) acc))))))]
    (f s 0 "")))

(string-X-2 "xplfxxx04xx")
(string-X-2 "xxHxix")
(string-X-2 "abxxxcd")
(string-X-2 "xabxxxcdx")
(string-X-2 "xKittenx")
(string-X-2 "Hello")
(string-X-2 "xx")
(string-X-2 "x")
(string-X-2 "")

(defn altPairs-1
  [s]
  (letfn [(f [s x]
            (if (== (count s) x)
              ""
              (if (or (== x 0) (== x 1)
                      (== x 4) (== x 5)
                      (== x 8) (== x 9))
                (str (subs s x (+ x 1))
                     (f s (inc x)))
                (f s (inc x)))))]
    (f s 0)))

(altPairs-1 "plf04")
(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "y")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [s]
  (letfn [(f [s x acc]
            (if (== (count s) x)
              acc
              (if (or (== x 0) (== x 1)
                      (== x 4) (== x 5)
                      (== x 8) (== x 9))
                (f s (inc x)
                   (str acc (subs s x (+ x 1))))
                (f s (inc x) acc))))]
    (f s 0 "")))

(altPairs-2 "plf04")
(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "y")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOther")

(defn string-Yak-1
  [s]
  (letfn [(f [s x]
            (if (empty? s)
              ""
              (if (and (= (first s) \y)
                       (= (first (rest s)) \a)
                       (= (first (rest (rest s))) \k)
                       (> (count s) 1))
                (f (rest (rest (rest s)))
                   (inc x))
                (str (first s)
                     (f (rest s) (inc x))))))]
    (f s 0)))

(string-Yak-1 "yakplf04yak")
(string-Yak-1 "yakpak")
(string-Yak-1 "pakyak")
(string-Yak-1 "yak123ya")
(string-Yak-1 "yak")
(string-Yak-1 "yakxxxyak")
(string-Yak-1 "HiyakHi")
(string-Yak-1 "xxxyakyyyakzzz")

(defn string-Yak-2
  [s]
  (letfn [(f [s x acc]
            (if (empty? s)
              acc
              (if (and (= (first s) \y)
                       (= (first (rest s)) \a)
                       (= (first (rest (rest s))) \k)
                       (> (count s) 1))
                (f (rest (rest (rest s)))
                   (inc x) acc)
                (f (rest s)
                   (inc x) (str acc (first s))))))]
    (f s 0 "")))

(string-Yak-2 "yakplf04yak")
(string-Yak-2 "yakpak")
(string-Yak-2 "pakyak")
(string-Yak-2 "yak123ya")
(string-Yak-2 "yak")
(string-Yak-2 "yakxxxyak")
(string-Yak-2 "HiyakHi")
(string-Yak-2 "xxxyakyyyakzzz")

(defn has271-1
  [xs]
  (letfn [(f [xs]
            (if (or (<= (count xs) 2)
                    (empty? xs))
              false
              (if (and
                   (== (first (rest xs))
                       (+ (first xs) 5))
                   (<= (if (pos? (- (first (rest (rest xs)))
                                    (dec (first xs))))
                         (- (first (rest (rest xs)))
                            (dec (first xs)))
                         (* -1 (- (first (rest (rest xs)))
                                  (dec (first xs))))) 2))
                true
                (f (rest xs)))))]
    (f xs)))

(has271-1 [2 4 7 2 1])
(has271-1 [1 2 7 1])
(has271-1 [1 2 8 1])
(has271-1 [2 7 1])
(has271-1 [3 8 2])
(has271-1 [2 7 3])
(has271-1 [2 7 4])
(has271-1 [2 7 -1])
(has271-1 [2 7 -2])
(has271-1 [4 5 3 8 0])
(has271-1 [2 7 5 10 4])
(has271-1 [2 7 -2 4 9 3])
(has271-1 [2 7 5 10 1])
(has271-1 [2 7 -2 4 10 2])
(has271-1 [1 1 4 9 0])
(has271-1 [1 1 4 9 4 9 2])

(defn has271-2
  [xs acc]
  (letfn [(f [xs ac]
            (if (empty? xs)
              false
              (if (>= (count xs) 3)
                (if (and (== (- (first (rest xs))
                                (first xs)) 5)
                         (>= 2 (if (neg? (- (first (rest (rest xs)))
                                            (- (first xs) 1)))
                                 (* -1 (- (first (rest (rest xs)))
                                          (- (first xs) 1)))
                                 (* 1 (- (first (rest (rest xs)))
                                         (- (first xs) 1))))))
                  true
                  (f (rest xs) ac))
                (f (empty xs) ac))))]
    (f xs acc)))

(has271-2 [2 4 7 2 1 2 1 7] "")
(has271-2 [1 2 7 1] "")
(has271-2 [1 2 8 1] "")
(has271-2 [2 7 1] "")
(has271-2 [3 8 2] "")
(has271-2 [2 7 3] "")
(has271-2 [2 7 4] "")
(has271-2 [2 7 -1] "")
(has271-2 [2 7 -2] "")
(has271-2 [4 5 3 8 0] "")
(has271-2 [2 7 5 10 4] "")
(has271-2 [2 7 -2 4 9 3] "")
(has271-2 [2 7 5 10 1] "")
(has271-2 [2 7 -2 4 10 2] "")
(has271-2 [1 1 4 9 0] "")
(has271-2 [1 1 4 9 4 9 2] "")